//En este he sido ayudado un poco por chatgpt. El de la otra carpeta (que lo muestra por consola) si que es mio 100%.

class Interprete {

    //Propiedades
    #Nombre;
    #Apellidos;
    #NombreArtistico;

    constructor(nombre, apellidos, nombreArtistico) {

        this.#Nombre = nombre;
        this.#Apellidos = apellidos;
        this.#NombreArtistico = nombreArtistico;

    };

    getInfo() {

        return this.#Nombre +" " + this.#Apellidos + " (más conocido como " +this.#NombreArtistico+")";

    };
};

class Disco {

    //Propiedades:
    nombre;
    interprete;
    año;
    tipo;
    portada;
    #localizacion;
    #prestado;

    constructor() {

        this.nombre = "";
        this.interprete = new Interprete("", "", "");
        this.año = "";
        this.tipo = "";
        this.portada = "";
        this.#localizacion = 0;
        this.#prestado = false;

    };

    //Método que añade las propiedades del nuevo disco creado.
    incluirPropiedades(Nombre, Interprete, Año, Tipo, Portada, Localizacion) {

        this.nombre = Nombre;
        this.interprete = Interprete;
        this.año = Año;
        this.tipo = Tipo;
        this.portada = Portada;
        this.#localizacion = Localizacion
        this.#prestado = false;

    };

    getPrestado() {

        return this.#prestado

    };

    getLocalizacion() {

        return this.#localizacion

    };
}

//Función que pide la información del disco a añadir
function nuevoDisco() {

    var nombre = prompt("Nombre del disco:");
    var nombreInterprete = prompt("Nombre real del intérprete:");
    var nombreArtistico = prompt("Nombre artístico:");
    var año = prompt("Año de salida:");
    var tipo = prompt("Tipo de musica");
    var portada = prompt("Dirección de la portada:");
    var localizacion = prompt("Localización:");
    var discoNuevo = new Disco();
    var nuevoInterprete = new Interprete(nombreInterprete, "", nombreArtistico);
    discoNuevo.incluirPropiedades(nombre, nuevoInterprete.getInfo(), año, tipo, portada, localizacion);
    return discoNuevo;

};

//Función que pide el elemento por el cual se quiere ordenar.
function elementoAordenar() {

    let sortElement = prompt("Elemento a ordenar:");
    return sortElement;

};

//Función que pide el nombre del disco que se quiere eliminar.
function nombreEliminar() {

    let nombre = prompt("Nombre del disco a eliminar:");
    return nombre;

};

class Discografia {

    discos;

    constructor() {

        this.discos = [];

    }

    //Funcion que añade 5 discos
    añadirDiscosRecomendados() {

        let interprete1 = new Interprete("Pedro", "Domínguez", "Quevedo");
        let disco1 = new Disco();
        disco1.incluirPropiedades("'Donde quiero estar'", interprete1.getInfo(), "2023", "Pop urbano", "https://i.scdn.co/image/ab67616d00001e02efc1b8f6beda4abe848a84e0");

        let interprete2 = new Interprete("Mauro Ezequiel", "Lombardo Quiroga", "Duki");
        let disco2 = new Disco();
        disco2.incluirPropiedades("'Temporada de Reggaetón'", interprete2.getInfo(), "2022", "Trap / Reggaetón", "https://images.genius.com/bc9affe546b293350096f13ddff87eee.1000x1000x1.png");

        let interprete3 = new Interprete("Benito Antonio", "Martínez Ocasio", "Bad Bunny");
        let disco3 = new Disco();
        disco3.incluirPropiedades("'Un Verano Sin Ti'", interprete3.getInfo(), "2022", "Reggaetón", "https://m.media-amazon.com/images/I/91vkwlCj4mL._SL1500_.jpg");

        let interprete4 = new Interprete("Michael Anthony", "Torres Monger", "Myke Towers");
        let disco4 = new Disco();
        disco4.incluirPropiedades("'LYKE MIKE'", interprete4.getInfo(), "2021", "Reggaetón", "https://i.scdn.co/image/ab67616d0000b273c09ae45aa7c410ca6d194ded");

        let interprete5 = new Interprete("Enmanuel", "Gazmey", "Anuel AA");
        let disco5 = new Disco();
        disco5.incluirPropiedades("'LLNM2'", interprete5.getInfo(), "2022", "Trap", "https://i.scdn.co/image/ab67616d0000b273a726edfbadc78b7397c78eda");

        this.discos.push(disco1, disco2, disco3, disco4, disco5);
        this.mostrarDiscos();

    };

    //Primero vacia el div discos y luego crea cada uno de los discos que existen ahora
    mostrarDiscos() {

        let divTabla = document.getElementById("discos");
        //vaciar el dom
        while (divTabla.hasChildNodes()) {
            divTabla.removeChild(divTabla.lastChild);
        };

        this.discos.forEach((disco) => {

            let discografiaDiv = document.getElementById("discos");
            let divDisco = document.createElement("div");
            divDisco.classList.add("disco");
            let divImagen = document.createElement("div");
            divImagen.classList.add("portada");
            let divInfo = document.createElement("div");
            divInfo.classList.add("info");
            let info = "";
            for (const propiedad in disco) {
                if (propiedad == "portada") {
                    let image = document.createElement("img")
                    image.setAttribute("src", disco[propiedad])
                    divImagen.appendChild(image)
                }
                else {
                    info += `<p>${propiedad}: ${disco[propiedad]}</p>`
                }
            }
            divInfo.innerHTML = info
            divDisco.appendChild(divImagen)
            divDisco.appendChild(divInfo)
            discografiaDiv.appendChild(divDisco)
        });

    };

    //Pide el disco al usuario y lo añade llamando a la funcion posterior
    añadirDisco() {

        let discoNuevo = nuevoDisco();
        this.discos.push(discoNuevo);
        this.mostrarDiscos();

    };

    borrarDisco() {

        let nombreDisco = nombreEliminar();
        let aEliminar = this.discos.filter((disco) => disco.nombre == nombreDisco)[0];
        let indexAEliminar = this.discos.indexOf(aEliminar);
        if (indexAEliminar != -1) {
            this.discos.splice(indexAEliminar, 1);
        } else alert("No existe");
        this.mostrarDiscos();

    };

    ordenarDiscos() {

        let propiedad = elementoAordenar();
        this.discos.sort((a, b) => a[propiedad].localeCompare(b[propiedad]));
        this.mostrarDiscos();

    };
};

let discografia = new Discografia();
discografia.mostrarDiscos();